/*
 * Using only single-character output statements that output a hash mark, a space,
 * or an end-of-line, write a program that produces the following shape:
 * ########
 *  ######
 *   ####
 *    ##
 */

#include <iostream>

using std::cout;

int main()
{
    int size = 8;
    for (int i = 0; i < 4; i++) {
        // space
        for (int j = 0; j < i; j++)
            cout << ' ';
        // hash
        for (int k = 0; k < size; k++)
            cout << '#';
        size -= 2;
        cout << '\n';
    }

    return 0;
}
