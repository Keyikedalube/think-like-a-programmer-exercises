/*
 * Write a program that takes an identification number (including its check digit)
 * of length six and determines whether the number is valid under the Luhn formula.
 * The program must process each character before reading the next one.
 */

#include <iostream>

using std::cout;
using std::cin;

bool is_odd(int number)
{
    if (number % 2 != 0)
        return true;
    return false;
}

void double_digit(char &input)
{
    int number = input - '0';
    number *= 2;

    if (number > 9) {
        // split out number digit by digit from RHS
        int remainder_sum = 0;
        while (number > 0) {
            remainder_sum += number % 10;
            number = number / 10;
        }

        // re-assign input
        input = remainder_sum + '0';
    } else {
        // re-assign input
        input = number + '0';
    }
}

int main()
{
    const int size = 6;
    int number[size];
    char input;

    int i = 0;
    while (i < size) {
        cout << "Enter next number ("<< i+1 << " of " << size << "): ";
        cin >> input;

        // Double digit every second input
        // Since array starts from 0 we have to consider second input as an odd no
        if (is_odd(i))
            double_digit(input);

        number[i] = input - '0';
        i++;
    }

    // Print our processed id and at the same time calculate all digits total
    cout << "Processed ID: ";
    int total = 0;
    for (int i = 0; i < size; i++) {
        cout << number[i];
        total += number[i];
    }
    cout << "\n";

    cout << "Total: " << total << "\n";

    cout << "============\n";
    if (total % 10 == 0)
        cout << "Luhn valid :)\n";
    else
        cout << "Luhn invalid :(\n";
    cout << "============\n";

    return 0;
}
