/*
 * For our automobile class from the previous exercise, add a support method that
 * returns a complete description of the automobile object as a formatted string,
 * such as, "1957 Chevrolet Impala". Add a second support method that returns the
 * age of the automobile in years.
 */

#include <chrono>
#include <iostream>

using std::chrono::system_clock;
using std::cout;
using std::localtime;
using std::string;
using std::to_string;




class Automobile {
    string manufacturer;
    string model_name;
    int    model_year;

public:
    Automobile();
    Automobile(string, string, int);

    // Setters and getters
    void   set_manufacturer(string);
    string get_manufacturer();
    void   set_model_name(string);
    string get_model_name();
    void   set_model_year(int);
    int    get_model_year();

    // Support methods
    string get_automobile_data();
    int    get_automobile_age();
};

Automobile::Automobile()
{
    set_manufacturer ("");
    set_model_name   ("");
    set_model_year   (-1);
}

Automobile::Automobile(string manufacturer_name, string model_name, int model_year)
{
    set_manufacturer (manufacturer_name);
    set_model_name   (model_name);
    set_model_year   (model_year);
}

void Automobile::set_manufacturer(string manufacturer)
{
    this->manufacturer = manufacturer;
}

string Automobile::get_manufacturer()
{
    return manufacturer;
}

void Automobile::set_model_name(string model_name)
{
    this->model_name = model_name;
}

string Automobile::get_model_name()
{
    return model_name;
}

void Automobile::set_model_year(int model_year)
{
    this->model_year = model_year;
}

int Automobile::get_model_year()
{
    return model_year;
}

string Automobile::get_automobile_data()
{
    string result;

    result += to_string(get_model_year()) + ' ';
    result += get_manufacturer() + ' ';
    result += get_model_name()   + ' ';

    return result;
}

int Automobile::get_automobile_age()
{
    /*
     * How to get current date and time?
     * https://stackoverflow.com/questions/8343676/how-to-get-current-date-and-time
     */
    auto clock = system_clock::now();
    auto time  = system_clock::to_time_t(clock);
    auto parts = localtime(&time);

    int current_year = parts->tm_year + 1900;
    int age = current_year - get_model_year();

    return age;
}




int main()
{
    Automobile omni   ("Maruti Suzuki"                , "Maruti Van"     , 1984);
    Automobile bolero ("Mahindra and Mahindra Limited", "Mahindra Bolero", 2000);

    cout << "Data : " << omni.get_automobile_data() << "\n";
    cout << "Age  : " << omni.get_automobile_age() << " years\n\n";

    cout << "Data : " << bolero.get_automobile_data() << "\n";
    cout << "Age  : " << bolero.get_automobile_age() << " years\n";

    return 0;
}
