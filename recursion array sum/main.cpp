/*
 * Write a recursive function that is given an array of integers and the size of
 * the array as parameters. The function returns the sum of the integers in the array.
 */

#include <iostream>

using std::cout;

int array_sum(int array[], int size)
{
    if (size == 0)
        return 0;

    int sum = array[size-1];
    size--;

    sum += array_sum(array, size);

    return sum;
}

int main()
{
    int array[] = {0, 1, 2, 3};
//    int array[0];

    cout << "Sum is: " << array_sum(array, 4) << "\n";

    return 0;
}
