/*
 * In statistics, the mode of a set of values is the value that appears most often.
 * Write code that processes an array of survey data, where survey takers have
 * responded to a question with a number in the range 1–10, to determine the mode
 * of the data set.
 * For our purpose, if multiple modes exist, any may be chosen.
 */

#include <iostream>

using std::cout;
using std::endl;

void print_array(int array[], const int SIZE)
{
    for (int i = 0; i < SIZE; i++)
        cout << array[i] << ' ';

    cout << '\n';
}

int compare_func(const void *void_a, const void *void_b)
{
    int *int_a = (int *)void_a;
    int *int_b = (int *)void_b;

    return *int_a - *int_b;
}

int first_approach(int array[], int SIZE)
{
    int highest_frequency = 0;
    int current_frequency = 1;
    int most_frequent     = array[0];

    for (int i = 1; i < SIZE; i++) {
        current_frequency++;
        if (array[i] != array[i+1]) {
            if (current_frequency > highest_frequency) {
                highest_frequency = current_frequency;
                most_frequent     = array[i];
            }
            current_frequency = 0;
        }
    }

    return most_frequent;
}

int second_approach(int array[], int SIZE)
{
    // Histogram
    const int MAX_RESPONSE = SIZE;
    int histogram[MAX_RESPONSE];

    for (int i = 0; i < MAX_RESPONSE; i++)
        histogram[i] = 0;

    for (int i = 0; i < SIZE; i++)
        histogram[array[i] - 1]++;

    int most_frequent = 0;
    for (int i = 0; i < MAX_RESPONSE; i++) {
        if (histogram[i] > histogram[most_frequent])
            most_frequent = i;
    }
    most_frequent++;

    return most_frequent;
}

int main()
{
    int array[] = {3, 1, 5, 8, 6, 3, 4, 1, 3, 2};
    const int SIZE = sizeof(array) / sizeof(array[0]);

    cout << "Before sorting:\n";
    print_array(array, SIZE);

    qsort(array, SIZE, sizeof(int), compare_func);

    cout << "After sorting:\n";
    print_array(array, SIZE);

    cout << "Most frequent using first approach  : " << first_approach(array, SIZE) << endl;
    cout << "Most frequent using second approach : " << second_approach(array, SIZE) << endl;

    return 0;
}
