/*
 * In this problem, you will write a class with methods to store and manipulate
 * a collection of student records. A student record contains a student number
 * and a grade, both integers, and a string for the student name. The following
 * functions are to be implemented:
 * record_add    : This method takes a student number, name, and grade and adds
 *                 a new record with this data to the collection.
 * record_fetch  : This function takes a student number and retrieves the record
 *                 with that student number from the collection.
 * record_remove : This function takes a student number and removes the record
 *                 with that student number from the collection.
 * The collection can be of any size. The addRecord operation is expected to be
 * called frequently, so it must be implemented efficiently.
 */

#include <iostream>

using std::cout;
using std::endl;
using std::string;




class StudentRecord {
    string name;
    int    number;
    int    grade;

public:
    // Constructors
    StudentRecord();
    StudentRecord(string, int, int);

    // Getters and Setters
    string get_student_name();
    void   set_student_name(string);
    int    get_student_grade();
    void   set_student_grade(int);
    int    get_student_number();
    void   set_student_number(int);
};

StudentRecord::StudentRecord()
{
    set_student_name("");
    set_student_number(-1);
    set_student_grade(0);
}

StudentRecord::StudentRecord(string student_name, int student_id, int final_grade_score)
{
    set_student_name(student_name);
    set_student_number(student_id);
    set_student_grade(final_grade_score);
}

string StudentRecord::get_student_name()
{
    return name;
}

void StudentRecord::set_student_name(string name)
{
    this->name = name;
}

int StudentRecord::get_student_grade()
{
    return grade;
}

void StudentRecord::set_student_grade(int grade)
{
    if (grade >= 0 and grade <= 100)
        this->grade = grade;
}

int StudentRecord::get_student_number()
{
    return number;
}

void StudentRecord::set_student_number(int number)
{
    this->number = number;
}




class StudentCollection {
    typedef struct student_node_data {
        StudentRecord     data;
        student_node_data *next;
    } *student_node;

    student_node head;

    // Helper function
    void         delete_list(student_node);
    student_node copy_list(const student_node);

public:
    // Constructors and destructor
    StudentCollection();
    StudentCollection(const StudentCollection &);
    ~StudentCollection();

    // Class operation
    void          record_add(StudentRecord);
    StudentRecord record_fetch(int);
    void          record_remove(int);
    void          record_print();

    // Overload = operator
    StudentCollection &operator=(const StudentCollection &);
};

StudentCollection::StudentCollection()
{
    head = nullptr;
}

StudentCollection::StudentCollection(const StudentCollection &original)
{
    head = copy_list(original.head);
}

StudentCollection::~StudentCollection()
{
    delete_list(head);
}

void StudentCollection::delete_list(student_node current)
{
    while (current) {
        head = current->next;
        delete current;
        current = head;
    }
}

StudentCollection::student_node StudentCollection::copy_list(const student_node original)
{
    if (!original)
        return nullptr;

    student_node new_node_head = new student_node_data;
    new_node_head->data        = original->data;

    student_node original_iterator = original->next;
    student_node new_node_iterator = new_node_head;

    while (original_iterator) {
        new_node_iterator->next = new student_node_data;
        new_node_iterator       = new_node_iterator->next;
        new_node_iterator->data = original_iterator->data;
        original_iterator       = original_iterator->next;
    }

    new_node_iterator->next = original_iterator;

    return new_node_head;
}

void StudentCollection::record_add(StudentRecord data)
{
    student_node node = new student_node_data;
    node->data = data;
    node->next = head;

    head = node;
}

StudentRecord StudentCollection::record_fetch(int number)
{
    student_node current = head;

    while (current and current->data.get_student_number() != number)
        current = current->next;

    if (current)
        return current->data;
    else
        return StudentRecord("", -1, 0);
}

void StudentCollection::record_remove(int number)
{
    student_node current  = head;
    student_node previous = nullptr;

    while (current and current->data.get_student_number() != number) {
        previous = current;
        current  = current->next;
    }

    if (current) {
        if (!previous)
            head = current->next;
        else
            previous->next = current->next;

        delete current;
    }
}

void StudentCollection::record_print()
{
    student_node current = head;
    u_short      record  = 1;

    while (current) {
        cout << "Record " << record << ": ";
        cout << current->data.get_student_name()   << ' ';
        cout << current->data.get_student_number() << ' ';
        cout << current->data.get_student_grade()  << endl;

        current = current->next;
        record++;
    }
}

StudentCollection &StudentCollection::operator=(const StudentCollection &rhs)
{
    if (this != &rhs) {
        delete_list(head);
        head = copy_list(rhs.head);
    }

    return *this;
}




int main()
{
    StudentCollection list;

    StudentRecord s1("Fu", 101, 67);
    StudentRecord s2("Fa", 102, 87);
    StudentRecord s3("Fi", 103, 45);

    list.record_add(s1);
    list.record_add(s2);
    list.record_add(s3);

    StudentRecord who = list.record_fetch(103);
    cout << "Fetched: ";
    cout << who.get_student_name() << ' ' << who.get_student_number() << ' ' << who.get_student_grade();
    cout << "\n\n";

    StudentCollection copy_constructor(list);
    StudentCollection copy_assignment = list;

    list.record_remove(103);

    cout << "Records from list\n";
    list.record_print();
    cout << endl;

    cout << "Records from copy_constructor\n";
    copy_constructor.record_print();
    cout << endl;

    cout << "Records from copy_assignment\n";
    copy_assignment.record_print();
    cout << endl;

    return 0;
}
