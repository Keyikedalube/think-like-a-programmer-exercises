/*
 * To make the ciphertext problem even more challenging, have your program randomly
 * generate the cipher array instead of a hard-coded const array. Effectively,
 * this means placing a random character in each element of the array, but remember
 * that you can’t substitute a letter for itself. So the first element can’t be A,
 * and you can’t use the same letter for two substitutions—that is, if the first
 * element is S, no other element can be S.
 */

#include <iostream>

using std::cin;
using std::cout;
using std::string;

const int ROW = 26;
const int COL = 2;

char encode(char input, const char array[][COL])
{
    // Substitute the character and return it
    for (int i = 0; i < ROW; i++) {
        if (input == array[i][0])
            return array[i][1];
    }

    // Return the punctuation
    return input;
}

char decode(char input, const char array[][COL])
{
    // Substitute the character and return it
    for (int i = 0; i < ROW; i++) {
        if (input == array[i][1])
            return array[i][0];
    }

    // Return the punctuation
    return input;
}

int main()
{
    char array[ROW][COL];

    // Initialize the array
    for (int row = 0; row < ROW; row++) {
        for (int col = 0; col < COL; col++) {
            if (col == 0)
                array[row][col] = row + 'A';
            else {
                // Get a random character from A-Z
                char cipher = (random() % 26) + 65;
                int i = 0;
                while (i < row) {
                    if (cipher == array[row][0] or cipher == array[i][col]) {
                        cipher = (random() % 26) + 65;
                        i = -1;
                    }
                    i++;
                }
                array[row][col] = cipher;
            }
        }
    }

    // For printing all the contents of array
    // Testing purposes only
//    for (int row = 0; row < ROW; row++) {
//        for (int col = 0; col < COL; col++)
//            cout << array[row][col];
//        cout << '\n';
//    }

    // Get input
    char input;
    cout << "Enter the message: ";

    string encoded_message;
    string decoded_message;

    do {
        input = cin.get();

        // Cipher the message
        char result = encode(input, array);
        encoded_message.push_back(result);

        // Decipher the message again
        result = decode(result, array);
        decoded_message.push_back(result);

    } while (input != '\n');


    cout << '\n';
    cout << "Encoded message: " << encoded_message;
    cout << '\n';
    cout << "Decoded message: " << decoded_message;
    cout << '\n';

    return 0;
}
