/*
 * Are you disappointed we didn’t do more with sorting? I’m here to help.
 * To make sure you are comfortable with qsort, write code that uses the function
 * to sort an array of our student struct. First have it sort by grade, and then
 * try it again using the student ID.
 */

#include <iomanip>
#include <iostream>

using std::cout;
using std::endl;
using std::setw;

struct student {
    int id;
    int grade;
    /*
     * Reason why std::string wasn't used in this exercise
     * - https://stackoverflow.com/questions/40439356/how-to-use-qsort-to-sort-array-of-string-in-c
     * - https://stackoverflow.com/questions/6174955/what-kinds-of-types-does-qsort-not-work-for-in-c
     */
    const char *name;
};

void print_array(student *array, const int SIZE)
{
    cout << setw(3) << "ID" << setw(6) << "GRADE" << setw(6) << "NAME\n";
    for (int i = 0; i < SIZE; i++)
        cout << setw(3) << array[i].id << setw(5) << array[i].grade << "  " << array[i].name << '\n';
    cout << '\n';
}

int compare_grade(const void *void_a, const void *void_b)
{
    student *student_a = (student *)void_a;
    student *student_b = (student *)void_b;

    return (student_a->grade > student_b->grade) - (student_a->grade < student_b->grade);
}

int compare_id(const void *void_a, const void *void_b)
{
    student *student_a = (student *)void_a;
    student *student_b = (student *)void_b;

    return (student_a->id > student_b->id) - (student_a->id < student_b->id);
}

int main()

{
    const int SIZE = 10;

    student array[] = {
        {123, 100, "Sanen"},
        {300, 89 , "Theja"},
        {045, 78 , "Viki"},
        {987, 54 , "Kedau"},
        {301, 90 , "Kenken"},
        {013, 44 , "Bentt"},
        {402, 34 , "Morr"},
        {109, 89 , "Amen"},
        {612, 37 , "Zhuvi"},
        {350, 58 , "Rayat"}
    };

    cout << "Before sorting:\n";
    print_array(array, SIZE);

    qsort(array, SIZE, sizeof(student), compare_grade);

    cout << "After sorting by grade:\n";
    print_array(array, SIZE);

    qsort(array, SIZE, sizeof(student), compare_id);

    cout << "After sorting by student id:\n";
    print_array(array, SIZE);

    return 0;
}
