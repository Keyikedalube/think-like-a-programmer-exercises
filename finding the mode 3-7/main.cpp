/*
 * Write a program that is given an array of integers and determines the mode,
 * which is the number that appears most frequently in the array.
 */

#include <iostream>

using std::cout;
using std::endl;

void print_array(int *array, const int SIZE)
{
    for (int i = 0; i < SIZE; i++)
        cout << array[i] << ' ';

    cout << '\n';
}

int compare_func(const void *void_a, const void *void_b)
{
    int *int_a = (int *)void_a;
    int *int_b = (int *)void_b;

    return *int_a - *int_b;
}

int mode_histogram(int *array, int SIZE)
{
    int histogram[SIZE];

    // Initialize all histogram elements to 0
    for (int i = 0; i < SIZE; i++)
        histogram[i] = 0;

    for (int i = 0; i < SIZE; i++)
        histogram[array[i] - 1]++;

    int mode = 0;
    for (int i = 1; i < SIZE; i++)
        if (histogram[i] > histogram[mode])
            mode = i;
    mode++;

    return mode;
}

int main()
{
    const int SIZE = random() % 50;
    int array[SIZE];

    srand(time(NULL));

    // Assign random values to our array
    for (int i = 0; i < SIZE; i++)
        array[i] = random() % 10;

    cout << "Before sorting:\n";
    print_array(array, SIZE);

    qsort(array, SIZE, sizeof(int), compare_func);

    cout << "After sorting:\n";
    print_array(array, SIZE);

    cout << "Most frequent number using histogram approach: " << mode_histogram(array, SIZE) << endl;

    return 0;
}
