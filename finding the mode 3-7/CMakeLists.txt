cmake_minimum_required(VERSION 3.15)

project(finding-the-mode-3-7 LANGUAGES CXX)

add_executable(${PROJECT_NAME} main.cpp)
