/*
 * Write a program that uses only two output statements, cout << "#" and cout << "\n",
 * to produce a line of five hash symbols:
 * #####
 */

#include <iostream>

using std::cout;

int main()
{
    const int size = 5;

    for (int i = 1; i <= size; i++)
        cout << "#";
    cout << "\n";

    return 0;
}
