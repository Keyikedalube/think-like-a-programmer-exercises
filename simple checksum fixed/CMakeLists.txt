cmake_minimum_required(VERSION 3.15)

project(simple-checksum-fixed LANGUAGES CXX)

add_executable(${PROJECT_NAME} main.cpp)
