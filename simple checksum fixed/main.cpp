/*
 * Write a program that takes an identification number (including its check digit)
 * of length six and determines whether the number is valid under a simple formula
 * where the values of each digit are summed and the result checked to see whether
 * it is divisible by 10.
 * The program must process each character before reading the next one.
 */

#include <iostream>

using std::cout;
using std::cin;

int main()
{
    const int size = 6;
    int numbers[size];
    char input;

    // Get the digits
    for (int i = 0; i < size; i++) {
        cout << "Enter next number (" << i+1 << " of " << size << "): ";
        cin >> input;
        numbers[i] = input - '0';
    }

    // Sum the digits
    int total = 0;
    for (int i = 0; i < size; i++)
        total += numbers[i];

    // Total modulo 10
    if (total % 10 == 0)
        cout << "Simple checksum valid :)\n";
    else
        cout << "Simple checksum invalid :(\n";

    return 0;
}
