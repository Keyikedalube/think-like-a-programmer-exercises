cmake_minimum_required(VERSION 3.15)

project(class-5-1 LANGUAGES CXX)

add_executable(${PROJECT_NAME} main.cpp)
