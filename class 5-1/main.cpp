/*
 * Let’s try implementing a class using the basic framework. Consider a class to
 * store the data for an automobile. We’ll have three pieces of data: a manufacturer
 * name and model name, both strings, and a model year, an integer. Create a class
 * with get/set methods for each data member. Make sure you make good decisions
 * concerning details like member names. It’s not important that you follow my
 * particular naming convention. What’s important is that you think about the
 * choices you make and are consistent in your decisions.
 */

#include <iostream>

using std::cout;
using std::string;




class Automobile {
    string manufacturer;
    string model_name;
    int    model_year;

public:
    Automobile();
    Automobile(string, string, int);

    void   set_manufacturer(string);
    string get_manufacturer();
    void   set_model_name(string);
    string get_model_name();
    void   set_model_year(int);
    int    get_model_year();
};

Automobile::Automobile()
{
    set_manufacturer ("");
    set_model_name   ("");
    set_model_year   (-1);
}

Automobile::Automobile(string manufacturer_name, string model_name, int model_year)
{
    set_manufacturer (manufacturer_name);
    set_model_name   (model_name);
    set_model_year   (model_year);
}

void Automobile::set_manufacturer(string manufacturer)
{
    this->manufacturer = manufacturer;
}

string Automobile::get_manufacturer()
{
    return manufacturer;
}

void Automobile::set_model_name(string model_name)
{
    this->model_name = model_name;
}

string Automobile::get_model_name()
{
    return model_name;
}

void Automobile::set_model_year(int model_year)
{
    this->model_year = model_year;
}

int Automobile::get_model_year()
{
    return model_year;
}




int main()
{
    Automobile omni   ("Maruti Suzuki"                , "Maruti Van"     , 1984);
    Automobile bolero ("Mahindra and Mahindra Limited", "Mahindra Bolero", 2000);

    return 0;
}
