/*
 * Write a program to read a number character by character and convert it to an
 * integer, using just one char variable and two int variables.
 * The number will have either three or four digits.
 */

#include <iostream>

using std::cout;
using std::cin;

int main()
{
    int fourth_place = 0;
    int third_place  = 0;
    char input;

    cout << "Enter any number of three-four digits: ";
    input = cin.get();

    fourth_place += 1000 * (input-'0');
    third_place  += 100  * (input-'0');

    input = cin.get();

    fourth_place += 100 * (input-'0');
    third_place  += 10  * (input-'0');

    input = cin.get();

    fourth_place += 10 * (input-'0');
    third_place  += 1  * (input-'0');

    input = cin.get();

    if (input == 10)
        cout << third_place << "\n";
    else {
        fourth_place += 1 * (input-'0');
        cout << fourth_place << "\n";
    }

    return 0;
}
