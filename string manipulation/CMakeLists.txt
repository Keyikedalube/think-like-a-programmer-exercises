cmake_minimum_required(VERSION 3.15)

project(variable-length-string-manipulation LANGUAGES CXX)

add_executable(${PROJECT_NAME} main.cpp)
