/*
 * Write heap-based implementations for three required string functions:
 * append       : This function takes a string and a character and appends the
 *                character to the end of the string.
 * concatenate  : This function takes two strings and appends the characters of the
 *                second string onto the first.
 * characterAt  : This function takes a string and a number and returns the character
 *                at that position in the string (with the first character in the
 *                string numbered zero).
 * Write the code with the assumption that characterAt will be called frequently,
 * while the other two functions will be called relatively seldom.
 * The relative efficiency of the operations should reflect the calling frequency.
 */

#include <iomanip>
#include <iostream>

using std::cout;
using std::setw;

int array_length(char *our_string)
{
    int count = 0;
    while (our_string[count] != 0)
        count++;

    return count;
}

void append(char **our_string, char our_char)
{
    int our_string_length = array_length(*our_string);
    int new_string_length = our_string_length + 2;

    char *new_string = new char[new_string_length];
    int i = 0;
    while (i < our_string_length) {
        new_string[i] = (*our_string)[i];
        i++;
    }
    new_string[i++] = our_char;
    new_string[i] = 0;

    delete[] *our_string;
    *our_string = new_string;
}

void concatenate(char **first_string, char *second_string)
{
    int first_string_length  = array_length(*first_string);
    int second_string_length = array_length(second_string);
    int new_string_length    = first_string_length + second_string_length;

    char *new_string = new char[new_string_length + 1];

    // Append first string to new string
    for (int i = 0; i <= first_string_length; i++)
        new_string[i] = (*first_string)[i];

    // Append second string to new string
    for (int i = 0; i <= second_string_length; i++)
        new_string[first_string_length + i] = second_string[i];

    new_string[new_string_length] = 0;

    delete[] *first_string;
    *first_string = new_string;
}

char characterAt(char *our_string, int number)
{
    return our_string[number];
}

int main()
{
    char *our_string = new char[5];
    our_string[0] = 'H';
    our_string[1] = 'i';
    our_string[2] = 'y';
    our_string[3] = 'a';
    our_string[4] = 0;

    // characterAt operation
    cout << "characterAt : " << characterAt(our_string, 0) << '\n';

    // append operation
    append(&our_string, ',');
    cout << "append" << setw(8) << ": " << our_string << '\n';

    // concatenate operation
    char *second_string = new char[10];
    second_string[0] = ' ';
    second_string[1] = 't';
    second_string[2] = 'h';
    second_string[3] = 'e';
    second_string[4] = 'r';
    second_string[5] = 'e';
    second_string[6] = ' ';
    second_string[7] = ':';
    second_string[8] = ')';
    second_string[9] = 0;
    concatenate(&our_string, second_string);
    cout << "concatenate : " << our_string << '\n';

    delete[] our_string;
    delete[] second_string;

    return 0;
}
