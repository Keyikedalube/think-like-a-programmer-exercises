/*
 * In this problem, you will write functions to store and manipulate a collection
 * of student records. A student record contains a student number and a grade,
 * both integers. The following functions are to be implemented:
 * addRecord : This function takes a pointer to a collection of student records,
 *             a student number, and a grade, and it adds a new record with this
 *             data to the collection.
 * averageRecord : This function takes a pointer to a collection of student records
 *                 and returns the simple average of student grades in the collection
 *                 as a double.
 * The collection can be of any size. The addRecord operation is expected to be
 * called frequently, so it must be implemented efficiently.
 */

#include <iostream>

using std::cin;
using std::cout;

struct student_record {
    int number;
    int grade;
    student_record *next;
};

void addRecord(student_record **collection, int number, int grade)
{
    student_record *node = new student_record;

    node->number = number;
    node->grade  = grade;
    node->next   = *collection;

    *collection = node;

    node = NULL;
}

double averageRecord(student_record **collection)
{
    if (*collection == NULL)
        return 0.0;

    student_record *current = *collection;

    int    count   = 0;
    double average = 0.0;

    while (current != NULL) {
        count++;
        average += current->grade;

        current = current->next;
    }
    average /= count;

    return average;
}

int main()
{
    student_record *collection = NULL;
    for (int i = 0; i < 2; i++) {
        int number, grade;
        cout << "Enter student number and grade (separated by space): ";
        cin >> number >> grade;
        addRecord(&collection, number, grade);
    }

    student_record *tmp = collection;

    // Print our collection
//    while (tmp != NULL) {
//        cout << tmp->number << ' ' << tmp->grade << '\n';
//        tmp = tmp->next;
//    }
//    tmp = collection;

    // Get our collection's average
    cout << "Average is: " << averageRecord(&collection) << '\n';

    // Delete our collection
    while (collection != NULL) {
        tmp = tmp->next;
        delete collection;
        collection = tmp;
    }

    return 0;
}
