/*
 * If you’ve learned about binary numbers and how to convert from decimal to binary
 * and the reverse, try writing programs to do those conversions with unlimited
 * length numbers (but you can assume the numbers are small enough to be stored
 * in a standard C++ int).
 */

#include <algorithm>
#include <cmath>
#include <iostream>
#include <string>

using std::cerr;
using std::cin;
using std::cout;
using std::reverse;
using std::string;

int main()
{
    bool     is_binary = true;
    uint64_t number    = 0;
    char     input;

    cout << "Enter number to convert: ";
    input = cin.get();

    while (input != '\n') {
        ushort current_digit = input - '0';

        // Get information on whether we are converting
        // - decimal to binary or
        // - binary to decimal
        if (current_digit > 1 && is_binary)
            is_binary = false;

        /*
         * How to check if a number overflows an 'int'
         * https://stackoverflow.com/questions/13661481/how-to-check-if-a-number-overflows-an-int
         */
        if (number > UINT64_MAX/10 || (number == UINT64_MAX/10 && current_digit > UINT16_MAX%10)) {
            cerr << "Number overflow :( Aborting\n";
            return 1;
        } else
            number = number * 10 + current_digit;

        input = cin.get();
    }

    if (is_binary) {
        uint64_t converted_number = 0;

        cout << "Binary to decimal conversion is: ";

        int place = 0;
        while (number != 0) {
            converted_number += number % 10 * pow(2, place);
            place++;
            number /= 10;
        }

        cout << converted_number << '\n';
    } else {
        // Large number will result in a very long binary number
        // Best to store it in a string that doesn't has size limit unlike unsigned long int
        string binary;

        cout << "Decimal to binary conversion is: ";

        while (number != 0) {
            if (number % 2 != 0)
                binary.push_back('1');
            else
                binary.push_back('0');
            number /= 2;
        }

        reverse(begin(binary), end(binary));

        cout << binary << '\n';
    }

    return 0;
}
