cmake_minimum_required(VERSION 3.15)

project(positive-or-negative LANGUAGES CXX)

add_executable(${PROJECT_NAME} main.cpp)
