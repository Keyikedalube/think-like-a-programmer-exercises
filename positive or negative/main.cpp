/*
 * Write a program that reads 10 integers from the user.
 * After all the numbers have been entered, the user may ask to display the
 * count of positive numbers or the count of negative numbers.
 */

#include <iostream>

using std::cout;
using std::cin;

int main()
{
    const int size = 10;
    int numbers[size];

    int positive_count = 0;
    int negative_count = 0;

    // Get the 10 numbers
    int i = 1;
    while (i <= 10) {
        cout << "Enter a number (" << i << " of " << size << "): ";
        cin >> numbers[i];
        i++;
    }

    // Count positve and negative frequency
    i = 1;
    while (i++ <= 10)
        numbers[i] >= 0 ? positive_count++ : negative_count++;

    // Display choices and get input
    cout << "\nWhat do you want to do next?\n";
    cout << "1. Display positive number count\n";
    cout << "2. Display negative number count\n";
    cout << "Your choice: ";

    char choice;
    cin >> choice;

    // Final output
    switch (choice) {
    case '1':
        cout << "Positive number count: " << positive_count;
        break;
    case '2':
        cout << "Negative number count: " << negative_count;
        break;
    }

    cout << "\n";

    return 0;
}
