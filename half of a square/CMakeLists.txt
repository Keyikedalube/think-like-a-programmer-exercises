cmake_minimum_required(VERSION 3.15)

project(half-of-a-square LANGUAGES CXX)

add_executable(${PROJECT_NAME} main.cpp)

