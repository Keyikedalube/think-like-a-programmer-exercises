/*
 * Write a program that uses only two output statements, cout << "#" and cout << "\n",
 * to produce a pattern of hash symbols shaped like half of a perfect 5 x 5 square (or a right triangle):
 * #####
 * ####
 * ###
 * ##
 * #
 */

#include <iostream>

using std::cout;

int main()
{
    const int size = 5;

    for (int i = 1; i <= size; i++) {
        for (int j = size; j >= i; j--)
            cout << "#";
        cout << "\n";
    }

    return 0;
}
