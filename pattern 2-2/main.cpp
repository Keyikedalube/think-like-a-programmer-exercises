/*
 * Using only single-character output statements that output a hash mark, a space,
 * or an end-of-line, write a program that produces the following shape:
 *    ##
 *   ####
 *  ######
 * ########
 * ########
 *  ######
 *   ####
 *    ##
 */

#include <iostream>

using std::cout;

int main()
{
    int row_size = 4;
    int col_size = 8;

    int col_count   = 2;
    int space_count = 3;

    // Upper triangle
    int i = 1;
    while (i <= row_size) {
        for (int j = 1; j <= col_size; j++) {
            if (j <= space_count)
                cout << ' ';
            else if (col_count != 0) {
                cout << '#';
                col_count--;
            }
        }
        cout << '\n';

        i++;
        space_count--;
        col_count = i + i;
    }

    space_count = 0;

    // Lower triangle
    i = row_size;
    while (i >= 1) {
        for (int j = 1; j <= col_size; j++) {
            if (j <= space_count)
                cout << ' ';
            else if (col_count != 0) {
                cout << '#';
                col_count--;
            }
        }
        cout << '\n';

        i--;
        space_count++;
        col_count = i + i;
    }

    return 0;
}
