/*
 * Write a bool function that is passed an array and the number of elements in
 * that array and determines whether the data in the array is sorted.
 * This should require only one pass!
 */

#include <iostream>

using std::cout;

void print_array(int *array, const int SIZE)
{
    for (int i = 0; i < SIZE; i++)
        cout << array[i] << ' ';
    cout << '\n';
}

bool check_array_sorted(int *array, const int SIZE)
{
    const int NEW_SIZE = SIZE - 1;

    for (int i = 0; i < NEW_SIZE; i++)
        if (array[i] > array[i+1])
            return false;

    return true;
}

int main()
{
    srand(time(NULL));

    // Large size array won't help much
    const int SIZE = random() % 4;
    int array[SIZE];

    for (int i = 0; i < SIZE; i++)
        array[i] = random() % 40;

    print_array(array, SIZE);

    if (check_array_sorted(array, SIZE))
        cout << "Array is sorted :)";
    else
        cout << "Array is not sorted :(";

    cout << '\n';

    return 0;
}
