/*
 * Write a program that takes an identification number of arbitrary length and
 * determines whether the number is valid under the Luhn formula.
 * The program must process each character before reading the next one.
 */

#include <iostream>

using std::cout;
using std::cin;

bool is_odd(int number)
{
    if (number % 2 != 0)
        return true;
    
    return false;
}

int double_digit(char input)
{
    int number = input - '0';
    number *= 2;

    if (number > 9) {

        // split out number digit by digit from RHS
        int remainder_sum = 0;
        while (number > 0) {
            remainder_sum += number % 10;
            number = number / 10;
        }
        
        return remainder_sum;
    }
    
    return number;
}

void luhn_validate(int total)
{
    cout << "Total: " << total << "\n";

    cout << "============\n";
    if (total % 10 == 0)
        cout << "Luhn valid :)\n";
    else
        cout << "Luhn invalid :(\n";
    cout << "============\n";
}

int main()
{
    char input;

    cout << "Enter id no: ";
    input = cin.get();

    int odd_sum     = 0;
    int even_sum    = 0;
    int digit_count = 0;
    
    /*
     * Since digit_count starts from 0 we have to reverse odd/even digit doubling
     * Because 0 is a bit advantageous... we don't have to subtract our total
     * digit_count by 1 according to the book code solution
     * Also, while loop will correctly give the total digit_count with the extra
     * variable's increment before exiting the loop
     *
     * In practice, even position, alternate doubling goes to sum of odd and vice versa
     *
     * In our case, we are visually assuming 0 is 1 which is an odd number so the
     * double correctly goes to even number and vice versa
     */
    while (input != 10) {
        // Double digit every second input
        if (is_odd(digit_count)) {
            odd_sum += double_digit(input);
            even_sum += input - '0';
        } else {
            even_sum += double_digit(input);
            odd_sum += input - '0';
        }
        
        digit_count++;
        input = cin.get();
    }

    is_odd(digit_count) ? luhn_validate(odd_sum) : luhn_validate(even_sum);

    return 0;
}
