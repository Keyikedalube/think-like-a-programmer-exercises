/*
 * Write a recursive function that is given a singly linked list where the data
 * type is integer. The function returns the count of negative numbers in the list.
 */

#include <iostream>

using std::cout;

struct node {
    int   data;
    node *next;
};

int get_negative_count(node *list)
{
    // Handle special case when linked list is either empty or pointing to nullptr
    if (!list) return 0;

    // Head recursion
    int count = get_negative_count(list->next);
    if (list->data < 0) count++;

    return count;
}

int main()
{
    node *head = nullptr;

    // We want to generate pseudorandom numbers
    srand(time(nullptr));

    const int SIZE = rand() % 20;

    // Allocate random numbers of random size
    for (int i = 0; i < SIZE; i++) {
        node *current = new node();
        current->data = (rand() % 10) - 2;
        cout << current->data << ' ';
        current->next = head;
        head          = current;
    }
    cout << '\n';

    // Get and display negative count
    cout << "Negative numbers count: " << get_negative_count(head) << '\n';

    // Delete list
    while (head) {
        node *current = head->next;
        delete head;
        head = current;
    }

    return 0;
}
