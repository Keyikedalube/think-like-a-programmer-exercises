cmake_minimum_required(VERSION 3.15)

project(recursion-negative-count-list LANGUAGES CXX)

add_executable(${PROJECT_NAME} main.cpp)
