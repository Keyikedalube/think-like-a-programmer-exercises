cmake_minimum_required(VERSION 3.15)

project(char-to-int LANGUAGES CXX)

add_executable(${PROJECT_NAME} main.cpp)
