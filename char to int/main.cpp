/*
 * Write a program that reads a character from the user representing a digit,
 * 0 through 9.
 * Convert the character to the equivalent integer in the range 0–9,
 * and then output the integer to demonstrate the result.
 */

#include <iostream>

using std::cout;
using std::cin;

int main()
{
    char input;
    cout << "Enter a number (0-9): ";
    input = cin.get();

    // Convert char to int
    int number = input - '0';
    cout << "Number as int: " << number << "\n";

    return 0;
}
