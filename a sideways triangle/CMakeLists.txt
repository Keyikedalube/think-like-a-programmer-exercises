cmake_minimum_required(VERSION 3.15)

project(a-sideways-triangle LANGUAGES CXX)

add_executable(${PROJECT_NAME} main.cpp)
