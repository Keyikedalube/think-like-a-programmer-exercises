/*
 * Write a program that uses only two output statements, cout << "#" and cout << "\n",
 * to produce a pattern of hash symbols shaped like a sideways triangle:
 * #
 * ##
 * ###
 * ####
 * ###
 * ##
 * #
 */

#include <iostream>
#include <cmath>

using std::cout;

int main()
{
//    const int size = 4;

    /* Track the hashes
     * My solution
     */
//    // Up triangle
//    for (int i = 1; i <= size; i++) {
//        for (int j = 1; j <= i; j++)
//            cout << "#";
//        cout << "\n";
//    }

//    // Down triangle
//    for (int i = size-1; i >=1; i--) {
//        for (int j = 1; j <= i; j++)
//            cout << "#";
//        cout << "\n";
//    }

    /*
     * Track the empty spaces
     * Book solution
     */
    for (int row = 1; row <= 7; row++) {
        for (int column = 1; column <= 4-abs(4-row); column++)
            cout << "#";
        cout << "\n";
    }

    return 0;
}
