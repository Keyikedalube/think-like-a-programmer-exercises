/*
 * Write a program to read a number character by character and convert it to an
 * integer, using just one char variable and one int variable.
 * The number will have either three or four digits.
 */

#include <iostream>

using std::cout;
using std::cin;

int main()
{
    int  number = 0;
    char input;

    cout << "Enter any number of three-four digits: ";
    input = cin.get();

    number += 100 * (input-'0');

    input = cin.get();
    number += 10 * (input-'0');

    input = cin.get();
    number += input - '0';

    input = cin.get();
    if (input != 10)
        number = number * 10 + input - '0';

    cout << number << "\n";

    return 0;
}
