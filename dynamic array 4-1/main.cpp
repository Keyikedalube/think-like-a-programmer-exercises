/*
 * Design your own: Take a problem that you already know how to solve using an
 * array but that is limited by the size of the array.
 * Rewrite the code to remove that limitation using a dynamically allocated array.
 */

#include <iostream>

using std::cout;

void print_array(char *array, const int SIZE)
{
    for (int i = 0; i < SIZE; i++)
        cout << array[i] << ' ';
    cout << '\n';
}

int main()
{
    const int SIZE = 26;
    char *array = new char[SIZE];

    for (int i = 0; i < SIZE; i++)
        array[i] = i + 65;

    print_array(array, SIZE);

    // Resize array and insert all the small letter alphabets
    const int NEW_SIZE = SIZE + SIZE;
    char *new_array = new char[NEW_SIZE];

    for (int i = 0; i < SIZE; i++)
        new_array[i] = array[i];
    for (int i = SIZE; i < NEW_SIZE; i++)
        new_array[i] = i + 71;

    delete[] array;

    array     = new_array;
    new_array = NULL;

    print_array(array, NEW_SIZE);

    delete[] array;

    return 0;
}
