/*
 * Write a program that uses only two output statements, cout << "#" and cout << "\n",
 * to produce a pattern of hash symbols shaped like a perfect 5x5 square:
 * #####
 * #####
 * #####
 * #####
 * #####
 */

#include <iostream>

using namespace std;

int main()
{
    const int size = 5;

    for (int i = 1; i <= size; i++) {
        for (int j = size; j >= 1; j--)
            cout << "#";
        cout << "\n";
    }

    return 0;
}
