/*
 * A message has been encoded as a text stream that is to be read character by character.
 * The stream contains a series of comma-delimited integers, each a positive number
 * capable of being represented by a C++ int.
 * However, the character represented by a particular integer depends on the current decoding mode.
 * There are three modes: uppercase, lowercase, and punctuation.
 *
 * In uppercase mode, each integer represents an uppercase letter:
 * The integer modulo 27 indicates the letter of the alphabet (where 1 = A and so on).
 * So an input value of 143 in uppercase mode would yield the letter H because
 * 143 modulo 27 is 8 and H is the eighth letter in the alphabet.
 *
 * The lowercase mode works the same but with lowercase letters;
 * the remainder of dividing the integer by 27 represents the lowercase letter (1 = a and so on).
 * So an input value of 56 in lowercase mode would yield the letter b because
 * 56 modulo 27 is 2 and b is the second letter in the alphabet.
 *
 * In punctuation mode, the integer is instead considered modulo 9. See table in book.
 *
 * At the beginning of each message, the decoding mode is uppercase letters.
 * Each time the modulo operation (by 27 or 9, depending on mode) results in 0,
 * the decoding mode switches.
 * If the current mode is uppercase, the mode switches to lowercase letters.
 * If the current mode is lowercase, the mode switches to punctuation,
 * and if it is punctuation, it switches back to uppercase.
 */

#include <iostream>

using std::cout;
using std::cin;

int main()
{
    enum operation {
        UPPERCASE,
        LOWERCASE,
        PUNCTUATION
    };

    operation mode = UPPERCASE;
    
    char message;
    cout << "Enter the encoded message: ";

    // Cannot use while loop because message variable is still uninitialized
    // Using it works but we have to either turn off compiler warning or just let it be
    do {
        message = cin.get();
        int number = 0;
        while (message != ',' && message != 10) {
            number *= 10;
            number += message - '0';
            message = cin.get();
        }

        char decoded;

        switch (mode) {
        // For upper/lower case we need to subtract our number by 1 because
        // ASCII table start from 0
        // That way we can get our desired character, for instance, number 4
        // would give letter D and not E when subtracted by 1
        case UPPERCASE:
            number %= 27;
            if (number == 0) {
                mode = LOWERCASE;
                continue;
            }
            decoded = number + 'A' - 1;

            break;
        case LOWERCASE:
            number %= 27;
            if (number == 0) {
                mode = PUNCTUATION;
                continue;
            }
            decoded = number + 'a' - 1;

            break;
        case PUNCTUATION:
            number %= 9;
            switch (number) {
            case 1: decoded = '!'; break;
            case 2: decoded = '?'; break;
            case 3: decoded = ','; break;
            case 4: decoded = '.'; break;
            case 5: decoded = ' '; break;
            case 6: decoded = ';'; break;
            case 7: decoded = '"'; break;
            case 8: decoded = '\''; break;
            }
            if (number == 0) {
                mode = UPPERCASE;
                continue;
            }

            break;
        }

        cout << decoded;

    } while (message != 10);

    cout << "\n";

    return 0;
}
