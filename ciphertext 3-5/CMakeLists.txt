cmake_minimum_required(VERSION 3.15)

project(ciphertext-3-5 LANGUAGES CXX)

add_executable(${PROJECT_NAME} main.cpp)
