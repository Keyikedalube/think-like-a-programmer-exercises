/*
 * Have the previous program convert the ciphertext back to the plaintext to
 * verify the encoding and decoding
 */

#include <iostream>

using std::cin;
using std::cout;
using std::string;

const int ROW = 26;
const int COL = 2;

char encode(char input, const char array[][COL])
{
    // Substitute the character and return it
    for (int i = 0; i < ROW; i++) {
        if (input == array[i][0])
            return array[i][1];
    }

    // Return the punctuation
    return input;
}

char decode(char input, const char array[][COL])
{
    // Substitute the character and return it
    for (int i = 0; i < ROW; i++) {
        if (input == array[i][1])
            return array[i][0];
    }

    // Return the punctuation
    return input;
}

int main()
{
    const char array[][COL] = {
        // 1st Column: A-Z
        // 2nd Column: Cipher letter
        {'A', 'Z'},
        {'B', 'X'},
        {'C', 'V'},
        {'D', 'T'},
        {'E', 'R'},
        {'F', 'P'},
        {'G', 'N'},
        {'H', 'L'},
        {'I', 'J'},
        {'J', 'H'},
        {'K', 'F'},
        {'L', 'D'},
        {'M', 'B'},
        {'N', 'Y'},
        {'O', 'W'},
        {'P', 'U'},
        {'Q', 'S'},
        {'R', 'Q'},
        {'S', 'O'},
        {'T', 'M'},
        {'U', 'K'},
        {'V', 'I'},
        {'W', 'G'},
        {'X', 'E'},
        {'Y', 'C'},
        {'Z', 'A'}
    };

    // Get input
    char input;
    cout << "Enter the message: ";

    string encoded_message;
    string decoded_message;

    do {
        input = cin.get();

        // Cipher the message
        char result = encode(input, array);
        encoded_message.push_back(result);

        // Decipher the message again
        result = decode(result, array);
        decoded_message.push_back(result);

    } while (input != '\n');


    cout << '\n';
    cout << "Encoded message: " << encoded_message;
    cout << '\n';
    cout << "Decoded message: " << decoded_message;
    cout << '\n';

    return 0;
}
