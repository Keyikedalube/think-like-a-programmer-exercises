/*
 * Here’s a variation on the array of const values. Write a program for creating
 * a substitution cipher problem. In a substitution cipher problem, all messages
 * are made of uppercase letters and punctuation. The original message is called
 * the plaintext, and you create the ciphertext by substituting each letter with
 * another letter (for example, each C could become an X).
 * For this problem, hard-code a const array of 26 char elements for the cipher,
 * and have your program read a plaintext message and output the equivalent ciphertext.
 */

#include <iostream>

using std::cin;
using std::cout;

int main()
{
    const int ROW = 26;
    const int COL = 2;
    const char array[][COL] = {
        // 1st Column: A-Z
        // 2nd Column: Cipher letter
        {'A', 'Z'},
        {'B', 'X'},
        {'C', 'V'},
        {'D', 'T'},
        {'E', 'R'},
        {'F', 'P'},
        {'G', 'N'},
        {'H', 'L'},
        {'I', 'J'},
        {'J', 'H'},
        {'K', 'F'},
        {'L', 'D'},
        {'M', 'B'},
        {'N', 'Y'},
        {'O', 'W'},
        {'P', 'U'},
        {'Q', 'S'},
        {'R', 'Q'},
        {'S', 'O'},
        {'T', 'M'},
        {'U', 'K'},
        {'V', 'I'},
        {'W', 'G'},
        {'X', 'E'},
        {'Y', 'C'},
        {'Z', 'A'}
    };

    // Get input
    char input;
    cout << "Enter the message: ";

    // Cipher the message
    do {
        input = cin.get();

        // Print the substituted character
        bool is_character_substituted = false;
        for (int i = 0; !is_character_substituted and i < ROW; i++) {
            if (input == array[i][0]) {
                cout << array[i][1];
                is_character_substituted = true;
            }
        }

        // Print the punctuation
        if (!is_character_substituted and input != '\n')
            cout << input;
    } while (input != '\n');

    cout << '\n';

    return 0;
}
